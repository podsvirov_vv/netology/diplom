# Дипломный практикум в Yandex.Cloud

## Информация перед прочтением:

- Здесь перечислены все ссылка на текстовые документы по темам
- Так же перейдя на `Инициализация` можно пройтись по всем доментам находять внутри документа (через ссылки в конце документа)

## Перечень страниц от инифиализации до деплоия в k8s

- [Инициализация](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/00-initial-settings.md)

- [Создание облачной инфраструктуры](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/01-cloud-setup.md)

- [Создание Kubernetes кластера](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/02-k8s-setup.md)

- [Создание тестового приложения](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/03-create-application.md)

- [Подготовка cистемы мониторинга и деплой приложения](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/04-add-monitoring.md)

- [Установка и настройка CI/CD](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/05-add-ci-cd.md)

## Резльтат дипломной работы

- [Команды по воспроизведению](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/commands.md)

- [Конфигурация Terraform](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/tarraform.md)

- [Снимки экрана из Terraform Cloud](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/terraform-cloud.md)

- [Снимки экрана из Yandex Cloud](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/yandex-cloud.md)

- [Результат дипломной работы](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/06-final.md)
