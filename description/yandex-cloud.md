# Снимки экрана из Yandex Cloud

- Стенд Production

<img src=../image/cloud_prod.png>

- Стенд Stage

<img src=../image/cloud_stage.png>

- VM (Node K8s)

<img src=../image/node_1.png>

<img src=../image/node_2.png>

- VPC

<img src=../image/vpc.png>

<img src=../image/vpc1.png>

<img src=../image/vpc2.png>

- Loadbalancer

<img src=../image/loadbalancer.png>

- k8s

<img src=../image/k8s.png>

<img src=../image/k8s1.png>

- Node group

<img src=../image/node_group.png>

[Вернуться назад](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/README.md)