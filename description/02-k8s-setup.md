# Создание Kubernetes кластера

## Ожидаемый результат:

1. Работоспособный Kubernetes кластер.
2. В файле `~/.kube/config` находятся данные для доступа к кластеру.
3. Команда `kubectl get pods --all-namespaces` отрабатывает без ошибок.

- Для создание кластера буду использовать `Terraform`

- Конфигурация для `Yandex Managed Service for Kubernetes`

```
resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name                     = "k8s-cluster"
  description              = "cluster for diplom netology"
  folder_id                = local.cloud_folder_id[terraform.workspace]
  network_id               = "${yandex_vpc_network.k8s-cloud.id}"
  cluster_ipv4_range       = "10.111.0.0/16"
  service_ipv4_range       = "10.222.0.0/16"
  service_account_id       = "${yandex_iam_service_account.k8s-sa.id}"
  node_service_account_id  = "${yandex_iam_service_account.k8s-sa.id}"
  release_channel          = "STABLE"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s-key.id
  }

  master {

    version   = local.k8s_version
    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "saturday"
        start_time = "01:00"
        duration   = "2h"
      }

      maintenance_window {
        day        = "sunday"
        start_time = "01:00"
        duration   = "2h"
      }
    }

    regional {
      region = "ru-central1"

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-a.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-a.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-b.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-b.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-c.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-c.id}"
      }
    }
  }
}
```

- Конфигурация для `kubernetes node group`

```
resource "yandex_kubernetes_node_group" "nodes" {
  cluster_id  = "${yandex_kubernetes_cluster.k8s-cluster.id}"
  name        = "nodes"
  description = "nodes for cluster"
  version     = local.k8s_version

  instance_template {
    platform_id = "standard-v1"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.k8s-subnet-a.id}"]
    }

    resources {
      cores  = local.node_core[terraform.workspace]
      memory = local.node_memory[terraform.workspace]
    }

    boot_disk {
      type = "network-hdd"
      size = local.node_disk_size[terraform.workspace]
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "docker"
    }
  }

  scale_policy {
    auto_scale {
      min = local.node_min_replica[terraform.workspace]
      max = local.node_max_replica[terraform.workspace]
      initial = local.node_initial[terraform.workspace]
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "saturday"
      start_time = "01:00"
      duration   = "2h"
    }

    maintenance_window {
      day        = "sunday"
      start_time = "01:00"
      duration   = "2h"
    }
  }
}
```

- Добавление учетных данные кластера Kubernetes в `конфигурационный файл kubectl`

```
$ yc managed-kubernetes cluster get-credentials --id catkdickcuu52f7jr1j2 --external
```

- Проверка работоспособности кластера (готовности подов и нод)

```
$ kubectl get node

NAME                        STATUS   ROLES    AGE     VERSION
cl1ldnsc6cftjcjk2ubj-yxol   Ready    <none>   2m27s   v1.22.6


$ kubectl get pods --all-namespaces

NAMESPACE     NAME                                   READY   STATUS    RESTARTS   AGE
kube-system   coredns-5f8dbbff8f-h67xb               1/1     Running   0          7m16s
kube-system   ip-masq-agent-4d24m                    1/1     Running   0          2m38s
kube-system   kube-dns-autoscaler-598db8ff9c-xnmzc   1/1     Running   0          7m12s
kube-system   kube-proxy-x7r8r                       1/1     Running   0          2m38s
kube-system   metrics-server-7574f55985-2pl4h        2/2     Running   0          2m22s
kube-system   npd-v0.8.0-6h7ww                       1/1     Running   0          2m38s
kube-system   yc-disk-csi-node-v2-x4dj5              6/6     Running   0          2m38s
```

- Кластер готов к работе

Следующий этап: [Создание тестового приложения](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/03-create-application.md)
