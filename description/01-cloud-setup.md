# Создание облачной инфраструктуры

## Задачи:

1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя
2. Подготовьте backend для Terraform:
а. Рекомендуемый вариант: Terraform Cloud
б. Альтернативный вариант: S3 bucket в созданном ЯО аккаунте
3. Настройте workspaces
a.  Рекомендуемый вариант: создайте два workspace: stage и prod. В случае выбора этого варианта все последующие шаги должны учитывать факт существования нескольких workspace.
б. Альтернативный вариант: используйте один workspace, назвав его stage. Пожалуйста, не используйте workspace, создаваемый Terraform-ом по-умолчанию (default).
4. Создайте VPC с подсетями в разных зонах доступности.
5. Убедитесь, что теперь вы можете выполнить команды terraform destroy и terraform apply без дополнительных ручных действий.
6. В случае использования Terraform Cloud в качестве backend убедитесь, что применение изменений успешно проходит, используя web-интерфейс Terraform cloud.

## Ожидаемые результаты:

1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.
2. Полученная конфигурация инфраструктуры является предварительной, поэтому в ходе дальнейшего выполнения задания возможны изменения.

## Решение поставленных задач:

1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя

- Конфигурация создания сервисного аккаунта

```
resource "yandex_iam_service_account" "k8s-sa" {
  name = local.sa_name[terraform.workspace]
}
```

- В облаке создано две директории, поэтому в каждой из них будет уникальный сервисный аккаунт `name = local.sa_name[terraform.workspace]`

- Назначение сервисному аккаунту роли

```
resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 folder_id = local.cloud_folder_id[terraform.workspace]
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
 ]
}
```

2. Подготовьте backend для Terraform:

- В качестве backend буду использовать - `Terraform Cloud`

- Настройка backend

```
terraform {
  backend "remote" {
    organization = "podsvirov_vv"

    workspaces {
      prefix = "k8s-"
    }
  }
}
```

3. Настройте workspaces

- Буду использовать два workspace для stage и для production

- Создание workspace

```
$ terraform workspace new stage
$ terraform workspace new prod
```

- Создание lock файла

```
$ terraform providers lock -net-mirror=https://terraform-mirror.yandexcloud.net -platform=linux_amd64 -platform=darwin_arm64 yandex-cloud/yandex
```

- Инициализация `Terraform`

```
$ terraform init
```

4. Создайте VPC с подсетями в разных зонах доступности.

- Конфигурация `VPC`

```
resource "yandex_vpc_network" "k8s-cloud" {
  name = "k8s-cloud"
}
```

- Конфигурация `Subnet`

```
resource "yandex_vpc_subnet" "k8s-subnet-a" {
  name           = "k8s-subnet-a"
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-b" {
  name           = "k8s-subnet-b"
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-c" {
  name           = "k8s-subnet-c"
  v4_cidr_blocks = ["10.3.0.0/16"]
  zone           = "ru-central1-c"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}
```

5. Убедитесь, что теперь вы можете выполнить команды terraform destroy и terraform apply без дополнительных ручных действий.

- Команды выполняются без дополнительных ручных действий 

6. В случае использования Terraform Cloud в качестве backend убедитесь, что применение изменений успешно проходит, используя web-интерфейс Terraform cloud.

- Скриншоты представлены ниже

## Результат:

1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.

- Предварительная конфигурация `Terraform`

```
locals {
  cloud_id    = "b1gq90dgh25bebiu75o"
  zone        = "ru-central1-a"
  sa_name = {
    k8s-stage = "stage-terraform"
    k8s-prod = "prod-terraform"
  }
  cloud_folder_id = {
    k8s-stage = "b1gm3699f36daa94bfee"
    k8s-prod = "b1ggs10knq1uphnsfojn"
  }
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  backend "remote" {
    organization = "podsvirov_vv"

    workspaces {
      prefix = "k8s-"
    }
  }
}

provider "yandex" {
  cloud_id  = local.cloud_id
  folder_id = local.cloud_folder_id[terraform.workspace]
  zone      = local.zone
  token     = "${var.secret-token}"
}

resource "yandex_iam_service_account" "k8s-sa" {
  name        = local.sa_name[terraform.workspace]
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 folder_id = local.cloud_folder_id[terraform.workspace]
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
 ]
}

resource "yandex_vpc_network" "k8s-cloud" {
  name = "k8s-cloud"
}

resource "yandex_vpc_subnet" "k8s-subnet-a" {
  name           = "k8s-subnet-a"
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-b" {
  name           = "k8s-subnet-b"
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-c" {
  name           = "k8s-subnet-c"
  v4_cidr_blocks = ["10.3.0.0/16"]
  zone           = "ru-central1-c"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}
```

- Скриншот `Terraform cloud`

<img src=../image/workspace.png>

<img src=../image/cloud_stage.png>

<img src=../image/cloud_prod.png>

Следующий этап: [Создание Kubernetes кластера](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/02-k8s-setup.md)
