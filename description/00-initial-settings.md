## Перед созданием ресурсов

1. Установка Terraform [Инструкция](https://learn.hashicorp.com/tutorials/terraform/install-cli)

2. Установка `Интерфейса командной строки Yandex Cloud` [Инструкция](https://cloud.yandex.ru/docs/cli/quickstart#install)

3. Инициализация `Интерфейса командной строки Yandex Cloud`

```
$ yc init
```

Следующий этап: [Создание облачной инфраструктуры](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/01-cloud-setup.md)
