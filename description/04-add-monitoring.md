# Подготовка cистемы мониторинга и деплой приложения

## Цель:

1. Задеплоить в кластер `prometheus`, `grafana`, `alertmanager`, экспортер основных метрик `Kubernetes`.
2. Задеплоить тестовое приложение, например, `nginx сервер` отдающий статическую страницу.

## Ожидаемый результат:

1. Git репозиторий с конфигурационными файлами для настройки Kubernetes.
2. Http доступ к web интерфейсу grafana.
3. Дашборды в grafana отображающие состояние Kubernetes кластера.
4. Http доступ к тестовому приложению.

Следующий этап: [Подготовка cистемы мониторинга и деплой приложения](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/05-add-ci-cd.md)


## Рещение задач для выполения поставленных целей:

1. Задеплоить в кластер prometheus, grafana, alertmanager, экспортер основных метрик Kubernetes.

- Для деплоия `prometheus`, `alertmanager` и `node_exporter` воспользуюсь Helm чартом

```
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && helm repo update
```

- Создание окружения для мониторинга

```
$ kubectl create ns monitoring
```

- Установка чарта с `Prometheus`, `Alalertmanager` и `node_exporter`

```
$ helm -n monitoring install prometheus prometheus-community/prometheus
```

- Для отображения метрик в `Grafana` практически в реальном времени и снижения нагрузки на `Prometheus` установлю чарт `trickster`

```
$ helm repo add tricksterproxy https://helm.tricksterproxy.io && helm repo update
$ helm install -n monitoring trickster tricksterproxy/trickster -f ./charts/trickster_values.yaml
```

- Файл настройки для чарта `trickster` -> [Файл](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/charts/trickster_values.yaml)

- Файл деплоия `Grafana` -> [Файл](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/charts/grafana.yaml)

- Деплой `Grafana`

```
$ kubectl -n monitoring apply -f ./charts/grafana.yaml
```

- Файл деплоя тестового приложения -> [Файл](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/charts/app.yaml)

- Деплой тестового приложения 

```
$ kubectl -n site apply -f ./charts/app.yaml
```

## Результат:

1. Git репозиторий с конфигурационными файлами для настройки Kubernetes.

- [Git репозиторий](https://gitlab.com/podsvirov_vv/netology/diplom/-/tree/main/charts)

2. Http доступ к web интерфейсу grafana.

- http://178.154.230.251:3000

- Логин: `admin`

- Пароль: `netology`

3. Дашборды в grafana отображающие состояние Kubernetes кластера.

- http://178.154.227.218:3000/d/garysdevil-kube-state-metrics-v2/k8s-statemetrics

4. Http доступ к тестовому приложению.

- http://51.250.95.193:8888/

Следующий этап: [Установка и настройка CI/CD](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/05-add-ci-cd.md)
