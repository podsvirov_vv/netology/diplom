# Собранная конфигурация Terraform

```
locals {
  cloud_id    = "b1gq90dgh25bebiu75o"
  zone        = "ru-central1-a"
  k8s_version = "1.22"
  sa_name = {
    k8s-stage = "stage-terraform"
    k8s-prod = "prod-terraform"
  }
  cloud_folder_id = {
    k8s-stage = "b1gm3699f36daa94bfee"
    k8s-prod = "b1ggs10knq1uphnsfojn"
  }
  node_core = {
    k8s-stage = "2"
    k8s-prod = "4"
  }
  node_memory = {
    k8s-stage = "4"
    k8s-prod = "8"
  }
  node_disk_size = {
    k8s-stage = "64"
    k8s-prod = "128"
  }
  node_min_replica = {
    k8s-stage = "1"
    k8s-prod = "3"
  }
  node_max_replica = {
    k8s-stage = "5"
    k8s-prod = "10"
  }
  node_initial = {
    k8s-stage = "1"
    k8s-prod = "3"
  }
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }

  backend "remote" {
    organization = "podsvirov_vv"

    workspaces {
      prefix = "k8s-"
    }
  }
}

provider "yandex" {
  cloud_id  = local.cloud_id
  folder_id = local.cloud_folder_id[terraform.workspace]
  zone      = local.zone
  token     = "${var.secret-token}"
}

resource "yandex_iam_service_account" "k8s-sa" {
  name        = local.sa_name[terraform.workspace]
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 folder_id = local.cloud_folder_id[terraform.workspace]
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
 ]
}

resource "yandex_vpc_network" "k8s-cloud" {
  name = "k8s-cloud"
}

resource "yandex_vpc_subnet" "k8s-subnet-a" {
  name           = "k8s-subnet-a"
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-b" {
  name           = "k8s-subnet-b"
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-c" {
  name           = "k8s-subnet-c"
  v4_cidr_blocks = ["10.3.0.0/16"]
  zone           = "ru-central1-c"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name                     = "k8s-cluster"
  description              = "cluster for diplom netology"
  folder_id                = local.cloud_folder_id[terraform.workspace]
  network_id               = "${yandex_vpc_network.k8s-cloud.id}"
  cluster_ipv4_range       = "10.111.0.0/16"
  service_ipv4_range       = "10.222.0.0/16"
  service_account_id       = "${yandex_iam_service_account.k8s-sa.id}"
  node_service_account_id  = "${yandex_iam_service_account.k8s-sa.id}"
  release_channel          = "STABLE"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s-key.id
  }

  master {

    version   = local.k8s_version
    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "saturday"
        start_time = "01:00"
        duration   = "2h"
      }

      maintenance_window {
        day        = "sunday"
        start_time = "01:00"
        duration   = "2h"
      }
    }

    regional {
      region = "ru-central1"

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-a.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-a.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-b.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-b.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-c.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-c.id}"
      }
    }
  }
}

resource "yandex_kubernetes_node_group" "nodes" {
  cluster_id  = "${yandex_kubernetes_cluster.k8s-cluster.id}"
  name        = "nodes"
  description = "nodes for cluster"
  version     = local.k8s_version

  instance_template {
    platform_id = "standard-v1"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.k8s-subnet-a.id}"]
    }

    resources {
      cores  = local.node_core[terraform.workspace]
      memory = local.node_memory[terraform.workspace]
    }

    boot_disk {
      type = "network-hdd"
      size = local.node_disk_size[terraform.workspace]
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "docker"
    }
  }

  scale_policy {
    auto_scale {
      min = local.node_min_replica[terraform.workspace]
      max = local.node_max_replica[terraform.workspace]
      initial = local.node_initial[terraform.workspace]
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "saturday"
      start_time = "01:00"
      duration   = "2h"
    }

    maintenance_window {
      day        = "sunday"
      start_time = "01:00"
      duration   = "2h"
    }
  }
}
```
