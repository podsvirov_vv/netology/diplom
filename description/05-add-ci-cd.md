# Установка и настройка CI/CD

1. Получите токен сервисного аккаунта Kubernetes для аутентификации в GitLab

```
$ yc managed-kubernetes cluster get-credentials catkdickcuu52f7jr1j2 --external

$ kubectl apply -f ./charts/gitlab-admin-service-account.yaml

$ kubectl -n kube-system get secrets -o json | \
jq -r '.items[] | select(.metadata.name | startswith("gitlab-admin")) | .data.token' | \
base64 --decode

```

2. Установка gitlab runner

```
$ helm repo add gitlab https://charts.gitlab.io

$ helm install --namespace default gitlab-runner -f ./charts/gitlab-runner.yaml gitlab/gitlab-runner
```

3. Файл `.gitlab-ci.yml`

```
stages:
  - build
  - deploy

image: docker:20.10.5

services:
  - docker:20.10.5-dind

Builder && Push:
  stage: build
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:latest .
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker push ${CI_REGISTRY_IMAGE}:latest
  except:
    - tags

Builder Tags:
  stage: build
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} .
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
  only:
    - tags

Deploy:
  image: gcr.io/cloud-builders/kubectl:latest
  stage: deploy
  script:
    - kubectl config set-cluster k8s --server="$KUBE_URL" --insecure-skip-tls-verify=true
    - kubectl config set-credentials admin --token="$KUBE_TOKEN"
    - kubectl config set-context default --cluster=k8s --user=admin
    - kubectl config use-context default
    - sed -i "s/__VERSION__/gitlab-$CI_COMMIT_SHORT_SHA/" ./templates/k8s.yaml
    - kubectl apply -f ./templates/k8s.yaml
    - kubectl set image deployment/app app=${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
  only:
    - tags
  tags:
    - podsvirov_vv
```

4. URL задеплоенного приложения - http://51.250.95.193:8888/

Следующий этап: [Результат дипломной работы](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/06-final.md)