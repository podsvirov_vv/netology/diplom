# Создание тестового приложения

## Ожидаемый результат:

1. Git репозиторий с тестовым приложением и Dockerfile.
2. Регистр с собранным docker image. В качестве регистра может быть DockerHub или Yandex Container Registry, созданный также с помощью terraform.

- [Git репозиторий](https://gitlab.com/podsvirov_vv/netology/application)

- Приложение написано на `go`, будет отдавать статические данные `Test application podsvirov_vv`

```
package main

import (
	"fmt"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Test application podsvirov_vv")
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":80", nil))
}
```

- `Dockerfile`

```
FROM golang:1.11-alpine AS build

WORKDIR /src/
COPY /src/main.go go.* /src/
RUN CGO_ENABLED=0 go build -o /bin/vlad

FROM scratch
COPY --from=build /bin/vlad /bin/vlad
ENTRYPOINT [ "/bin/vlad" ]
```

- [Ссылка на образ в Docker Hub](https://hub.docker.com/repository/docker/kezan86/nginx_netology/general)


Следующий этап: [Подготовка cистемы мониторинга и деплой приложения](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/04-add-monitoring.md)