## Перечень выполняемых команд

```
$ yc init
$ terraform workspace new stage
$ terraform workspace new prod
$ terraform providers lock -net-mirror=https://terraform-mirror.yandexcloud.net -platform=linux_amd64 -platform=darwin_arm64 yandex-cloud/yandex
$ terraform init
$ terraform apply
$ yc managed-kubernetes cluster get-credentials --id catkdickcuu52f7jr1j2 --external
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts 
$ helm repo add tricksterproxy https://helm.tricksterproxy.io
$ helm repo add gitlab https://charts.gitlab.io
$ helm repo update
$ kubectl create ns monitoring
$ helm -n monitoring install prometheus prometheus-community/prometheus
$ helm install -n monitoring trickster tricksterproxy/trickster -f ./charts/trickster_values.yaml
$ kubectl -n monitoring apply -f ./charts/grafana.yaml
$ kubectl create ns site
$ kubectl -n site apply -f ./charts/app.yaml
$ kubectl apply -f ./charts/gitlab-admin-service-account.yaml
$ kubectl -n kube-system get secrets -o json | \
jq -r '.items[] | select(.metadata.name | startswith("gitlab-admin")) | .data.token' | \
base64 --decode
$ helm install --namespace default gitlab-runner -f ./charts/gitlab-runner.yaml gitlab/gitlab-runner
```