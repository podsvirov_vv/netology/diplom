# Результат дипломной работы

### Задачи:

1. Репозиторий с конфигурационными файлами `Terraform` и готовность продемонстрировать создание всех ресурсов с нуля.
2. Пример `pull request` с комментариями созданными `atlantis'ом` или снимки экрана из `Terraform Cloud`.
3. Репозиторий с конфигурацией `ansible`, если был выбран способ создания `Kubernetes` кластера при помощи `ansible`.
4. Репозиторий с `Dockerfile` тестового приложения и ссылка на собранный `docker image`.
5. Репозиторий с конфигурацией `Kubernetes` кластера.
6. Ссылка на `тестовое приложение` и веб интерфейс `Grafana` с данными доступа.
7. Все репозитории рекомендуется хранить на одном ресурсе ([github](https://github.com/), [gitlab](https://gitlab.com/))

### Решение поставленных задач:

1. Репозиторий с конфигурационными файлами `Terraform` и готовность продемонстрировать создание всех ресурсов с нуля.

- [Репозиторий c конфигурационными файлами Terraform](https://gitlab.com/podsvirov_vv/netology/diplom/-/tree/main/terraform)

- [Собранная конфигурация Terraform](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/tarraform.md)

- [Демонстрация создания всех ресурсов с нуля](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/commands.md)

2. Пример `pull request` с комментариями созданными `atlantis'ом` или снимки экрана из `Terraform Cloud`.

- [Снимки экрана из Terraform Cloud](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/description/terraform-cloud.md)

3. Репозиторий с конфигурацией `ansible`, если был выбран способ создания `Kubernetes` кластера при помощи `ansible`.

- Кластер создавал при помощи `Terraform` и `Yandex Managed Service for Kubernetes`, `kubernetes node group`

4. Репозиторий с `Dockerfile` тестового приложения и ссылка на собранный `docker image`.

- [Ссылка на репозиторий с Dockerfile](https://gitlab.com/podsvirov_vv/netology/application)

- [Ссылка на собранный образ Docker Hub](https://hub.docker.com/repository/docker/kezan86/nginx_netology/general)

5. Репозиторий с конфигурацией `Kubernetes` кластера.

- Для настройки использовал Helm чарты

- [Настройка для Trickster](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/charts/trickster_values.yaml)

- [Сетап Grafana](https://gitlab.com/podsvirov_vv/netology/diplom/-/blob/main/charts/grafana.yaml)

- `Prometheus`, `Alalertmanager` и `node_exporter` установлены чартом

```
$ helm -n monitoring install prometheus prometheus-community/prometheus
```

6. Ссылка на `тестовое приложение` и веб интерфейс `Grafana` с данными доступа.

- [CI/CD тестового приложения](https://gitlab.com/podsvirov_vv/netology/application/-/pipelines)

- [URL тестового приложения](http://84.201.159.173/)

- [Интервейс графана](http://178.154.227.218:3000)

- [Дашборд Grafana](http://178.154.227.218:3000/d/garysdevil-kube-state-metrics-v2/k8s-statemetrics?orgId=1)

- Логин: `admin`
- Пароль: `netology`

7. Все репозитории рекомендуется хранить на одном ресурсе ([github](https://github.com/), [gitlab](https://gitlab.com/))

- Все репозитории расположены на [Gitlab](https://gitlab.com/)

- [Основной раздел Дипломной работы](https://gitlab.com/podsvirov_vv/netology/diplom)

- [Тестовое приложение](https://gitlab.com/podsvirov_vv/netology/application)
