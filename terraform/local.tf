locals {
  cloud_id    = "b1gq90dgh25bebiu75o"
  zone        = "ru-central1-a"
  k8s_version = "1.22"
  sa_name = {
    k8s-stage = "stage-terraform"
    k8s-prod = "prod-terraform"
  }
  cloud_folder_id = {
    k8s-stage = "b1gm3699f36daa94bfee"
    k8s-prod = "b1ggs10knq1uphnsfojn"
  }
  node_core = {
    k8s-stage = "2"
    k8s-prod = "4"
  }
  node_memory = {
    k8s-stage = "4"
    k8s-prod = "8"
  }
  node_disk_size = {
    k8s-stage = "64"
    k8s-prod = "128"
  }
  node_min_replica = {
    k8s-stage = "1"
    k8s-prod = "3"
  }
  node_max_replica = {
    k8s-stage = "5"
    k8s-prod = "10"
  }
  node_initial = {
    k8s-stage = "1"
    k8s-prod = "3"
  }
}