resource "yandex_vpc_network" "k8s-cloud" {
  name = "k8s-cloud"
}

resource "yandex_vpc_subnet" "k8s-subnet-a" {
  name           = "k8s-subnet-a"
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-b" {
  name           = "k8s-subnet-b"
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}

resource "yandex_vpc_subnet" "k8s-subnet-c" {
  name           = "k8s-subnet-c"
  v4_cidr_blocks = ["10.3.0.0/16"]
  zone           = "ru-central1-c"
  network_id     = "${yandex_vpc_network.k8s-cloud.id}"
}
