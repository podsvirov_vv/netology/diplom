terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  cloud_id  = local.cloud_id
  folder_id = local.cloud_folder_id[terraform.workspace]
  zone      = local.zone
  token     = "${var.secret-token}"
}
