resource "yandex_iam_service_account" "k8s-sa" {
  name        = local.sa_name[terraform.workspace]
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 folder_id = local.cloud_folder_id[terraform.workspace]
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
 ]
}
