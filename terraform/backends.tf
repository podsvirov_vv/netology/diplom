terraform {
  backend "remote" {
    organization = "podsvirov_vv"

    workspaces {
      prefix = "k8s-"
    }
  }
}
