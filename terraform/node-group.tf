resource "yandex_kubernetes_node_group" "nodes" {
  cluster_id  = "${yandex_kubernetes_cluster.k8s-cluster.id}"
  name        = "nodes"
  description = "nodes for cluster"
  version     = local.k8s_version

  instance_template {
    platform_id = "standard-v1"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.k8s-subnet-a.id}"]
    }

    resources {
      cores  = local.node_core[terraform.workspace]
      memory = local.node_memory[terraform.workspace]
    }

    boot_disk {
      type = "network-hdd"
      size = local.node_disk_size[terraform.workspace]
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "docker"
    }
  }

  scale_policy {
    auto_scale {
      min = local.node_min_replica[terraform.workspace]
      max = local.node_max_replica[terraform.workspace]
      initial = local.node_initial[terraform.workspace]
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "saturday"
      start_time = "01:00"
      duration   = "2h"
    }

    maintenance_window {
      day        = "sunday"
      start_time = "01:00"
      duration   = "2h"
    }
  }
}
