resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name                     = "k8s-cluster"
  description              = "cluster for diplom netology"
  folder_id                = local.cloud_folder_id[terraform.workspace]
  network_id               = "${yandex_vpc_network.k8s-cloud.id}"
  cluster_ipv4_range       = "10.111.0.0/16"
  service_ipv4_range       = "10.222.0.0/16"
  service_account_id       = "${yandex_iam_service_account.k8s-sa.id}"
  node_service_account_id  = "${yandex_iam_service_account.k8s-sa.id}"
  release_channel          = "STABLE"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s-key.id
  }

  master {

    version   = local.k8s_version
    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "saturday"
        start_time = "01:00"
        duration   = "2h"
      }

      maintenance_window {
        day        = "sunday"
        start_time = "01:00"
        duration   = "2h"
      }
    }

    regional {
      region = "ru-central1"

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-a.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-a.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-b.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-b.id}"
      }

      location {
        zone      = "${yandex_vpc_subnet.k8s-subnet-c.zone}"
        subnet_id = "${yandex_vpc_subnet.k8s-subnet-c.id}"
      }
    }
  }
}
